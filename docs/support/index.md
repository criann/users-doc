---
title: "Infos de contact"
---
# Support aux utilisateurs

## CRIANN

Pour joindre le support technique du CRIANN :

- Par courrier électronique : [support@criann.fr](mailto:support@criann.fr)
- Par téléphone : [+33 (0)2 32 91 42 91](tel:+33232914291)

### Horaires du standard

- Lundi au jeudi : 9h00 - 12h00, 14h00 - 17h30
- Vendredi : 9h00 - 12h00, 14h00 - 16h30

## SYVIK

Pour joindre le centre d’exploitation et de supervision du [réseau SYVIK](https://www.criann.fr/reseau-syvik/) :

- Par courrier électronique : [noc@crt.syvik.fr](mailto:noc@crt.syvik.fr)
- Par téléphone : [+33 (0)9 70 20 76 27](tel:+33970207627)
- Portail de supervision SYVIK de l’exploitant : [https://portail.crt.syvik.fr](https://portail.crt.syvik.fr)
