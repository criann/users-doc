# Hébergement

Le CRIANN propose différentes offres d'hébergement de services à ses utilisateurs.

## Hébergement physique

Le CRIANN dispose de plusieurs salles pour l'hébergement d'équipements informatiques pouvant être installés en armoire au standard 19 pouces.

En fonction du besoin d'hébergement différents niveaux de services sont disponibles :

- accès autonome en 24/7 ou en heures ouvrées uniquement ;
- alimentation simple ou redondée ;
- secours court sur onduleur uniquement ou secours long avec groupe électrogène.
  
Tous les bâtiments d'hébergement sont en accès contrôlé avec vidéosurveillance et possèdent les systèmes de climatisation adaptés pour l'hébergement de services informatiques.

Pour toute demande d'hébergement dans nos salles, vous pouvez contacter le [support du CRIANN](mailto:support@criann.fr?subject=%5BCRIANN%5D%20Demande%20d%27h%C3%A9bergement%20physique)

---

## Machines virtuelles

Le CRIANN dispose d'une infrastructure de virtualisation permettant de fournir, dans des cas spécifiques, l'hébergement de machines virtuelles dédiées pour ses utilisateurs.

---

## Plateforme web mutualisée

La plateforme web mutualisée du CRIANN repose sur des solutions open-source (Apache HTTPd, PHP) en environnement Linux.

À l'acceptation de votre demande, vous recevrez des instructions ainsi que des identifiants et mots de passe pour :

- réaliser des transferts de fichiers via SFTP ;
- l'accès à une base de donnée de type MySQL ;
- consulter les statistiques de consultation de votre site.

Un quota disque, de l'ordre de 3&nbsp;Go, est associé à chaque site hébergé. Celui-ci peut être augmenté, de manière raisonnable, en fonction de vos besoins en formulant une demande à notre [support](../support/index.md).

---

## Zones DNS

Le CRIANN dispose de plusieurs serveurs DNS pouvant être configurés pour héberger vos zones DNS en tant que serveur primaire et/ou secondaire.

L'édition (ajout/modification/suppression d'entrées DNS) des zones hébergées sur nos serveurs maître passe par une demande auprès de notre [support](../support/index.md)
