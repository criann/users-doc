---
title: "Autres services"
---
# Les autres services du CRIANN

## Miroirs logiciel

Le CRIANN gére un serveur qui héberge des copies miroirs de distributions Linux et de différents logiciels open-source pour ses propres besoins et ceux de ses usagers.

Parmi les miroirs, vous pourrez trouver des miroirs de différentes distributions comme Debian, Ubuntu, CentOS, PHP, etc.

Nos miroirs sont disponibles aux URLs suivantes :

- `http://ftp.crihan.fr/mirrors/`
- `ftp://ftp.crihan.fr/mirrors/`
- `rsync://ftp.crihan.fr`

Les miroirs des distributions suivantes sont utilisables en l'état pour vos installations et mises à jour :

- Ubuntu : `http://ftp.crihan.fr/ubuntu`
- Debian : `http://ftp.crihan.fr/debian`
- CentOS : `http://ftp.crihan.fr/centos/8/BaseOS/x86_64/os/` (par exemple pour CentOS 8 en version 64 bits)
