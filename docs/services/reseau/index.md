# Réseau SYVIK

## Modalités de raccordement

Le réseau régional SYVIK offre une couverture complète du territoire régional au travers de points de présence localisés sur les principales agglomérations ainsi qu'un service de collecte multi-opérateurs.

Pour toute demande ou étude de raccordement de votre établissement sur le réseau SYVIK, vous pouvez prendre contact avec le [support du CRIANN](mailto:support@criann.fr?subject=%5BSYVIK%5D%20Demande%20de%20raccordement).

## Service d'exploitation

Les demandes de modifications, les déclarations de travaux et d'incident doivent être adressées au NOC SYVIK.

Les informations de contact pour le NOC sont disponibles sur la page de [contact](../../support/index.md).

## Supervision

L'exploitant du réseau met en place un portail qui permet aux usagers d'avoir accès à l'ensemble des tickets en cours ainsi qu'à des données de supervision.

- portail de l'exploitant : [Portail SYVIK](https://portail.crt.syvik.fr)

En complément, le CRIANN, en tant que coordinateur du réseau SYVIK, opère des services disjoints de supervision :

- un serveur de supervision de liens réseaux : [Cacti](https://stats.syvik.fr/cacti)
- un serveur de supervision Intermapper : [Intermapper](http://intermapper.crihan.fr) (carte graphique de représentation du réseau et supervision de certains services)

Ces services sont restreints par des identifiants/mots de passe.
