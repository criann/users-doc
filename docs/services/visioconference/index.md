---
title: 'Réserver des ressources'
---
# Service de visioconférence

## Description du service

Le CRIANN met à disposition de ses utilisateurs un service permettant l'organisation de visioconférences multipoint accessibles au travers de stations de visioconférence (H.323 / SIP), d'un poste de travail ou d'une simple ligne téléphonique. 

Les ressources actuellement accessibles pour nos utilisateurs sur la plateforme Pexip :

|               Fonctionnalités               |                                                           Pexip                                                            |
| :-----------------------------------------: | :------------------------------------------------------------------------------------------------------------------------: |
|               Partage d'écran               |                                       :fontawesome-regular-check-circle:{: .green }                                        |
|  Connexion via des terminaux IP H.323/SIP   |                                       :fontawesome-regular-check-circle:{: .green }                                        |
|     Connexion via un poste téléphonique     |                                       :fontawesome-regular-check-circle:{: .green }                                        |
|      Connexion via un poste de travail      |                         :fontawesome-regular-check-circle:{: .green }<br>(Linux, Mac OS, Windows)                          |
|  Connexion via un navigateur web (WebRTC)   |                                       :fontawesome-regular-check-circle:{: .green }                                        |
| Streaming et enregistrement des conférences |                             :fontawesome-regular-check-circle:{: .green }<br>(sous conditions)                             |
| Allocation de numéros fixes de conférences  |                             :fontawesome-regular-check-circle:{: .green }<br>(sous conditions)                             |
|             Capacité vidéo max              |                                                         40 canaux                                                          |
|             Capacité audio max              |                                     40 canaux<br>dont 30 canaux téléphones classiques                                      |
|  Nombre max de participants par conférence  |                                    env. 30% (15 participants) de la capacité vidéo max                                     |
|           Portail de réservation            | [Pexip &nbsp;&nbsp;&nbsp;:fontawesome-solid-calendar-alt:](https://visio-resa.syvik.fr){: .md-button .md-button--primary } |

!!! note "Service en mode best effort"

    Le service Pexip est proposé en mode best effort, ce qui veut dire que les réservations sont acceptées mais sans certifier que tous les participants pourront se connecter, c'est le principe du premier arrivé premier servi.

### Obtenir un compte organisateur Pexip

Pour obtenir un compte qui vous permettra de faire des réservations de salles virtuelles sur la plateforme Pexip, il vous faut nous envoyer un mail à [support@criann.fr](mailto:support@criann.fr?subject=%5BVISIO%5D%20Demande%20dun%20compte%20pour%20organiser%20des%20visioconf%C3%A9rences%20Pexip&body=Bonjour%2C%0A%0AJe%20souhaite%20un%20compte%20pour%20r%C3%A9server%20des%20visioconf%C3%A9rences%20Pexip.%0AVoici%20ci-dessous%20les%20informations%20demand%C3%A9es.%0A%0ANom%3A%0APr%C3%A9nom%3A%0AEmail%3A%0AOrganisation%3A%0AComposant%2FLaboratoire%3A){: target=_blank } en précisant :

- nom
- prénom
- email
- organisation de tutelle et si possible votre composante/laboratoire

La demande sera alors étudiée en heures ouvrées par notre service et une réponse vous sera retournée dans les plus brefs délais.
