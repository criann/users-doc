---
title: Participer
authors: 
    - Sébastien VIGNERON <sebastien.vigneron@criann.fr>
---
# Participer à une conférence programmée

!!! info "Pour participer"
    L'organisateur de la conférence vous a transmis les paramètres de connexion.
    
    Il n'est pas nécessaire d'avoir un compte organisateur pour participer à des conférences réservées sur la plateforme Pexip.

!!! success "Recommandations"
    Pour utiliser le service de visioconférence dans les meilleures conditions possibles, il est recommandé :

    - de disposer d’une connexion réseau d’au moins 1&nbsp;Mbit/s
    - d’effectuer un test de connexion préalable pour vérifier la bonne reconnaissance de votre micro et caméra
    - d’utiliser un casque pour une meilleure expérience audio
    - de couper systématiquement votre micro lorsque vous ne prenez pas la parole

---

## Plateforme **Pexip**

Vous pouvez vous connecter selon différentes modalités :

- un navigateur web compatible WebRTC (Google Chrome de préférence)
    - URL de connexion : [https://visio.syvik.fr](https://visio.syvik.fr)
    - vérifier le support WebRTC de votre navigateur sur [https://whatwebcando.today](https://whatwebcando.today)
        - la fonction **Real-Time Communication** doit indiquer **YES**
    - liste des [navigateurs et versions compatibles [EN]](https://docs.pexip.com/clients/using_web_admin.htm)
- le client [Pexip Infinity Connect](https://www.pexip.com/apps#hs_cos_wrapper_widget_1609399310400){: target="_blank" }
    - format de l'adresse appelée : `{MeetingID}@visio.syvik.fr`
    - [Ordinateur avec processeurs x86_64](https://www.pexip.com/apps#hs_cos_wrapper_widget_1609399310400){: target="_blank" } : les [prérequis et installation [EN]](https://docs.pexip.com/clients/using_desktop_admin.htm)
    - [Apple iOS 10.x et suivants](https://apps.apple.com/us/app/pexip/id1195088102){: target="_blank" }
    - [Android 7.0 et suivants](https://play.google.com/store/apps/details?id=com.pexip.infinityconnect){: target="_blank" }
- une station de visioconférence ou client logiciel H.323 ou SIP
    - H323 : `visio.syvik.fr` puis `{MeetingID}#` puis un éventuel `{codePIN}#`
    - SIP : `{MeetingID}@visio.syvik.fr`
- un poste téléphonique
    - [02 32 91 09 96](tel:+33232910996) puis `{MeetingID}#` puis un éventuel `{codePIN}#`
