---
title: "En cas de problème"
---
# Important

!!! note
    La plupart des problèmes de connexion sont liés :
    
    - au fait d'avoir mal configuré le client ou 
    - d'avoir un filtrage réseau qui bloque les flux réseaux.
    
    Veuillez consultez votre service informatique et vérifier l'état du filtrage sur votre poste et sur votre réseau local **_avant_** de prendre contact avec notre service technique.

!!! success "Réaliser un test préliminaire avant la conférence"
    Il est conseiller d'effectuer un test de connexion avant la conférence à laquelle vous avez été invité afin de valider que tout est OK. Bien sûr, il faut faire le test dans les mêmes conditions que le jour J (même réseau local, même filtrage et même terminal [ordinateur ou station de visioconférence]).

## Pexip Infinity Connect et clients WebRTC

Les adresses IP des serveurs de visioconférence sont :

- IPv4 : `195.221.21.101` à `195.221.21.114`
- IPv6 : `2001:660:7401:211::101` à `2001:660:7401:211::114`

### Salle de test

Une salle de test est disponible afin de valider l'installation du client ainsi que les ouvertures de ports.

Il vous suffit de lancer un appel vers `test@visio.syvik.fr` depuis le client Pexip Infinity Connect.

### Les flux réseau

**_Flux en sortie_**

Les ports et protocoles suivants sont utilisés de votre terminal vers nos serveurs de visioconférence.

| Source   | Port src    | Destination | Port dst    | Protocole | Usage      |
| -------- | ----------- | ----------- | ----------- | --------- | ---------- |
| Terminal | &lt;any&gt; | Serveurs    | 443         | TCP       | HTTPS      |
| Terminal | &lt;any&gt; | Serveurs    | 1720        | TCP       | H.323 SIG  |
| Terminal | &lt;any&gt; | Serveurs    | 5060        | TCP       | SIP        |
| Terminal | &lt;any&gt; | Serveurs    | 5061        | TCP       | SIP/TLS    |
| Terminal | &lt;any&gt; | Serveurs    | 33000-39999 | TCP       | H.323 SIG  |
| Terminal | &lt;any&gt; | Serveurs    | 40000-49999 | TCP/UDP   | RTP / RTCP |

**_Flux en entrée_**

Les ports et protocoles suivants sont utilisés de nos serveurs de visioconférence vers votre terminal.

| Source   | Port src    | Destination | Port dst    | Protocole | Usage      |
| -------- | ----------- | ----------- | ----------- | --------- | ---------- |
| Serveurs | 33000-39999 | Terminal    | 1720        | TCP       | H.323 SIG  |
| Serveurs | 33000-39999 | Terminal    | 5060        | TCP       | SIP        |
| Serveurs | 33000-39999 | Terminal    | 5061        | TCP       | SIP/TLS    |
| Serveurs | 33000-39999 | Terminal    | &lt;any&gt; | TCP       | H.323 SIG  |
| Serveurs | 40000-49999 | Terminal    | &lt;any&gt; | TCP/UDP   | RTP / RTCP |
