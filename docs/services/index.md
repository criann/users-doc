
# Les services

Le CRIANN propose des services informatiques de différentes natures à ses usagers.

## Réseaux informatiques

- [Réseau régional SYVIK](reseau/index.md)

## Calcul numérique

- [Calcul Haute Performance](calcul/index.md)
    - [Cluster en production (MYRIA)](calcul/cluster-myria/index.md)
- [Modélisation Moléculaire](calcul/modelisation_moleculaire.md)

## Services applicatifs

- [Visioconférence](visioconference/index.md)
- [Hébergement](hebergement.md)
- [Messagerie électronique](messagerie/index.md)
    - [Filtrage antispam/antivirus](messagerie/antispam.md)
    - [Hébergement de boites email](messagerie/mail-hosting/index.md)
