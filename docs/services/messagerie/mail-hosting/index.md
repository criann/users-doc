---
title: 'Description'
---
# Hébergement de boites email

Le CRIANN propose à ses usagers un service d'hébergement des boites email associées à un domaine.

Vous pouvez prendre contact avec le [service technique du CRIANN](mailto:support@criann.fr?subject=%5BCRIANN%5D%20H%C3%A9bergement%20courrier%20%C3%A9lectronique) pour une demande d'hébergement de courrier électronique pour votre domaine.

Si vous souhaitez faire une demande d'ouverture d'un nouveau compte de messagerie électronique pour un domaine déjà hébergé au CRIANN, vous pouvez renvoyer le formulaire disponible sur notre [page Formulaires](https://www.criann.fr/formulaires) dûment rempli à <admin@criann.fr>.
