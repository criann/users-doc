# Utiliser le service

## Depuis un client de messagerie

!!! important "Format de votre identifiant"
    Votre identifiant de messagerie email est de la forme "`uid`@`domaine.tld`" (par exemple `jdoe@criann.fr`) alors que l'adresse email de réception/émission est usuellement de la forme "`prenom.nom`@`domaine.tld`" (par exemple `john.doe@criann.fr`).  
    Veuillez donc veiller à bien définir votre identité pour l'envoi de vos mails (voir la note en bas de cette page) sinon vos correspondant ne pourront répondre à vos mails.

Pour configurer votre client de messagerie, vous trouverez dans le tableau ci-après l'ensemble des paramètres pour consulter et envoyer des messages : 

| protocole | serveur          | port                       | chiffrement                     | méthode d'authentification | 
| --------- | ---------------- |--------------------------- | ------------------------------- | -------------------------- | 
| IMAP      | imap.syrhano.net | 993<br>(Thunderbird : 143) | SSL<br>(Thunderbird : StartTLS) | Mot de passe en clair      | 
| SMTP      | smtp.syrhano.net | 587                        | SSL<br>(Thunderbird : StartTLS) | Mot de passe en clair      | 

???+ warning "Support des clients de messagerie récents"
    L'utilisation des dernières versions de client de messagerie peut poser problème en particulier sur l'établissement de connexions chiffrées.
    C'est le cas par exemple avec les dernières versions de Mozilla Thunderbird. Vous trouverez ci-dessous les informations pour paramétrez le client afin de supporter la connexion avec nos serveurs.

    === "Activer le support de TLSv1.0 sous Thunderbird"
    
        - Préférences (sous le menu Édition sous certains OS, ou dans Outils sous le nom Paramètres)
        - Général
        - Bouton `Éditeur de configuration`
        - Chercher `security.tls.version.min`, cela indique `3` dans les clients récents
        - Modifier cette valeur par `1` afin de supporter TLSv1.0

        <http://kb.mozillazine.org/security.tls.version.*>

## Depuis un navigateur web

Vous pouvez accéder à votre boite email depuis le webmail <https://webmail.syrhano.net> en utilisant votre identifiant (de la forme "`uid`@`domaine.tld`") et le mot de passe associé.

!!! warning "Vérifier l'identité de votre compte sous le webmail"
    Lors de votre premier usage du webmail ou lorsqu'il y a un changement de votre adresse email, vérifiez et corrigez au besoin l'identité que vous utilisez pour émettre des emails via le webmail.  
    En effet le webmail est configuré par défaut avec votre identifiant de connexion comme email, hors cet identifiant n'est pas un email valide (voir la note en haut de cette page).

    - Préférences
    - Onglet Identité
    - Champs "courriel" : adresse email que vous souhaitez utiliser à l'envoi (doit être une adresse email valide de votre boite sinon vos correspondants ne sauront comment vous répondre).
