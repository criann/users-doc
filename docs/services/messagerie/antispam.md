# Filtrage antispam/antivirus

Le CRIANN opère un cluster de boîtiers de filtrage antispam/antivirus qui redirigent les emails acceptés vers les services qui hébergent les boites email.

La gestion des emails entrants se fait par étape :

- vérification de la réputation de l'IP de la machine qui a contacté notre service
- filtrage par les moteurs antispam et antivirus
- routage de l'email vers le service qui héberge la boite email

## Vérification de la réputation

Le cluster utilise une base centralisée afin de protéger notre service des adresses IP considérées comme émettrices de SPAM ou de malware. Vous pouvez consulter la réputation d'une adresse IP via ce service à l'URL suivante : [SenderBase](https://talosintelligence.com/)

Il est possible, sur demande à notre [support](../../support/index.md), d'ajouter des adresses IP en liste blanche.

## Règles de filtrage

Une règle de filtrage antispam est appliquée, pour chaque domaine, parmi la liste des règles suivantes :

- `SPAM and SUSPECTED SPAM erased / MARKETING tagged` : les SPAM et suspectés SPAM sont supprimés et les emails de marketing ont leurs sujets préfixés
- `SPAM erased / SUSPECTED SPAM and MARKETING tagged` : les SPAM sont supprimés et les emails suspectés d'être du SPAM ou de marketing ont leurs sujets préfixés
- `SPAM erased / SUSPECTED SPAM tagged` : les SPAM sont supprimés et les emails suspectés d'être du SPAM ont leurs sujets préfixés
- `SPAM and SUSPECTED SPAM tagged` : les SPAM et les emails suspectés d'être du SPAM ont leurs sujets préfixés
- `SPAM tagged (header)` : une entête email `X-Spam-Status: yes` est ajouté en cas de SPAM détecté

L'email entrant, une fois le filtrage antispam réalisé, passe dans le moteur antivirus.  
Les boîtiers, après filtrage antispam et antivirus, redirige les emails vers le serveur désigné en charge de l'hébergement des boites email (service CRIANN ou extérieur).

!!! info
    Le système de filtrage limite la taille des emails en réception à 20&nbsp;Mo.

## Redirection après traitement

Les emails, une fois traités, sont retransmis par les boîtiers vers leurs destinations finales en utilisant les adresses suivantes :

- `193.52.218.20` (`a1.relay.syrhano.net`)
- `193.52.218.30` (`b1.relay.syrhano.net`)

Nous pouvons appliquer un routage statique vers une ou plusieurs destinations (adresses IP ou FQDN) ou effectuer un routage dynamique par consultation d'un service LDAP distant contenant l'information.
