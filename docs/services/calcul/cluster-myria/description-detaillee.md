---
title: Architecture
---
# Description de la configuration

## Matériel

Le calculateur Myria est une solution ATOS BULL d’une puissance de 419 TFlops Xeon, 327 TFlops GPU et 27 TFlops Xeon Phi KNL :

- **11304 cœurs de calcul** (10664 Xeon, 640 Xeon Phi KNL)
- **366 nœuds de calcul bi-sockets Broadwell** (28 cœurs à 2,4 GHz, 128 Go de RAM DDR4 à 2400 MHz), dont :
    - 12 nœuds dotés chacun 20 To de disques internes
    - 1 nœud doté de 20 To de disques internes et d'1 To de RAM
    - 12 nœuds dotés chacun de 2 cartes **GPU Kepler K80** (4 unités de traitement GPU par nœud, 12 Go de mémoire embarquée par unité)
    - 8 nœuds dotés chacun de 2 cartes **GPU Pascal P100** (2 unités de traitement GPU par nœud, 12 Go de mémoire embarquée par unité)
    - 1 nœud doté de 3 cartes GPU Pascal P100 (3 unités de traitement GPU, 12 Go de mémoire embarquée par unité)
- **5 nœuds de calcul bi-sockets SkyLake** (16 cœurs à 2,1 GHz, 187 Go de RAM DDR4 à 2666 MHz) **dotés chacun de 4 cartes GPU Volta V100-SXM2** (4 unités de traitement GPU par nœud interconnectées par **NVLink2, 32 Go de mémoire** embarquée par unité)
- **10 nœuds de calcul dotés chacun d'un Xeon Phi KNL** 7210 (64 cœurs à 1,3 GHz et 96 Go de RAM DDR4 à 2133 MHz par nœud, 16 Go de mémoire rapide MCDRAM embarquée dans le processeur KNL)
- **1 nœud SMP** avec 256 cœurs Haswell à 2,2 GHz et 4 To de RAM DDR4 à 2133 MHz
- **2 nœuds de visualisation** (bi-sockets Broadwell) dotés chacun d'une carte GPU K80 et de 256 Go de RAM DDR4 à 2400 MHz
- 5 nœuds de connexion (bi-sockets Broadwell) dotés chacun de 256 Go de RAM DDR4 à 2400 MHz
- 53 To de mémoire DDR4
- Réseau d'interconnexion Intel Omni-Path à faible latence et haut débit (100 Gbit/s)
- 2,5 Po d'espace disque (`/home`, `/dlocal` et `/save`)
- Connexion 4 x 10 Gbit/s et 1 x 40 Gbit/s sur le réseau SYVIK

## Environnement logiciel

- Environnement Linux 64 bits (CentOS 7.6)
- Soumission des travaux avec Slurm
- Système de fichiers parallèle IBM Spectrum Scale (GPFS)
- Environnement de développement :
    - Compilateurs Intel 2017 et 2019, Gnu 4.8.5 Fortran, C, C++ (support OpenMP)
    - Librairie Intel MPI 2017 et 2019 (support de la norme MPI-3)
    - Librairies mathématiques Intel MKL (dont BLAS, LAPACK et ScaLAPACK), FFTW 3.3.5
    - API CUDA C/C++ 8.0, 9.0, 9.1, 10.0, 10.1, 10.2 et 11.1, OpenACC 2.6 (Pgi 20.7), librairies CUBLAS et CUFFT pour accélération sur GPU
    - Exemples de Makefiles : `/soft/makefiles`
