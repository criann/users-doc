# Visualisation à distance

**_Service de visualisation des données à distance permettant les pré et post-traitements au moyen de logiciels graphiques_**

Pré-requis : Installer la dernière version du client TurboVNC sur votre poste de travail [http://www.turbovnc.org/](http://www.turbovnc.org/).

## Étape 1 : démarrage d'une session graphique

**_A partir d'une frontale de Myria_** : demandez une session de visualisation graphique en tapant la commande : `startvisu`

Une aide est disponible en ajoutant `--help` dans la ligne de commande.

Ce script soumet un calcul dans Slurm, en file d'attente dédiée aux sessions de visualisation. Deux serveurs équipés de 2 GPU K80 et de 256&nbsp;Go de mémoire chacun accueillent les sessions. Ces GPU sont utilisés par l'affichage graphique, ils ne sont pas prévus pour être exploités par les calculs.

Une fois le calcul démarré, un message s'affiche dans votre fenêtre de commande avec les paramètres à donner à votre client TurboVNC sur votre poste de travail

**_Maj du 26.03.2020_** : ajout d'une 3ème serveur accessible via la commande : `startvisubis`

**Attention**, par défaut les sessions réservent "que" 16&nbsp;Go. Pour demander plus de mémoire, utiliser l'option `--mem`.
ex : `startvisubis --mem 30G`

Cette commande accède à un serveur gpu_k80 reconditionné en serveur de visualisation. Il est équipé de 4 GPU K80 dédiés à l'affichage graphique et de 128&nbsp;Go de mémoire.

## Étape 2 : connexion à la session

**_Sur votre poste de travail_** : lancer le client TurboVNC viewer et saisissez les paramètres de serveur, de display ainsi que le mot de passe.

Une fenêtre "bureau" doit apparaitre. La barre de tâche en bas de fenêtre permet d'ouvrir un terminal.

## Étape 3 : fin de la session

La fermeture de la fenêtre TurboVNC Viewer ne termine pas la session. Vous pouvez donc vous y reconnecter en utilisant les mêmes paramètres.

Pour terminer la session, il faut soit se déloguer (clic-droit sur la barre du bas, puis "Log Out") ou tuer le job Slurm associé (commande `scancel` avec le numéro de calcul).

Comme pour tout calcul, au bout de la durée du calcul "TimeLimit" (par défaut 4&nbsp;h), le calcul est tué : la session et tout ce qu'elle contient sont détruits.

## Session par défaut

Par défaut, les ressources suivantes sont associées :

- Wallclock : 4&nbsp;h
- Nb coeurs CPU : 4
- Mémoire : 62,5&nbsp;Go

Si vous avez besoin de plus de ressources, consulter les options de startvisu en utilisant l'argument `--help`.

Cas particulier de `startvisubis` :

- Wallclock : 4&nbsp;h
- Nb coeurs CPU : 3
- Mémoire : 16&nbsp;Go

## Logiciels

Les logiciels, comme Paraview, Visit ou Blender, sont accessibles via la gestion des modules. Tapez la commande `module avail` pour connaître la liste.

Remarque : les logiciels graphiques utilisant les librairies OpenGL doivent être lancés via la commande `vglrun` : par exemple, pour lancer Paraview, après avoir chargé le module correspondant `(Exemple : module load paraview)`, il faut taper `vglrun paraview`.

## Quantité de mémoire utilisée

Les sessions de visualisation sont des calculs Slurm comme les autres. Il est donc possible de consulter le rapport de consommation généré par Slurm.

- exemple : `sacct -o "MaxVMSize,MaxRSS,AllocGRES,JobID%20,User,Partition" --start 2020-03-23 --end 2020-03-26 --partition visu,visu2`

Plus d'informations sur la commande `sacct` dans la documentation détaillée

## Retrouver les informations d'une session

Les informations de la session sont sauvegardées dans le fichier `~/.visu/slurm-jobid.out` en remplaçant `jobid` par le numéro du calcul.

Correspondance des 2 mots de passes :

- passu (mode control) 
- passr (mode view only) 

IP publiques des 2 serveurs :

- visu1 : myria-visu1.criann.fr = 195.221.27.73
- visu2 : myria-visu2.criann.fr = 195.221.27.74

**Remarque** : ne pas oublier de spécifier le numéro de la session avec le `:` après l'IP du serveur.