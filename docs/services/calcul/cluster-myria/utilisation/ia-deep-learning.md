# IA - Deep Learning

## Logithèque disponible

 Les frameworks de Deep Learning (tensorflow, pytorch, keras) et de machine learning (scikit-learn) sont installés sous environnements python3. Ces environnements proposent également des outils de traitement d’images, tels que opencv et scikit-image, ainsi que des outils de d’analyse des données tel que pandas.

Il existe quatre installations principales disponibles sur Myria.

- La première installation propose les frameworks en version figée et basés sur cuda 9.0.  
  L’environnement est accessible en chargeant le module python3-DL/3.6.1 : `module load python3-DL/3.6.1`
  - Les installations 3.6.9 et 3.7.6 , **recommandées pour un usage standard** proposent les frameworks dans des versions relativement récentes et sont basés respectivement sur cuda 10.0 et cuda 10.1  
  Ces environnements sont accessibles en chargeant le module python3-DL/3.6.9 : `module load python3-DL/3.6.9` (resp. python3-DL/3.7.6 : `module load python3-DL/3.7.6`)
- Enfin la quatrième installation propose les frameworks dans leur version de novembre 2020 et est basée sur cuda 10.1.  
  L’environnement est accessible en chargeant le module python3-DL/3.8.5 : `module load python3-DL/3.8.5`

Le tableau suivant récapitule les différentes versions disponible des frameworks pour chacune de ces deux versions.

| module       | python3-DL/3.6.1 | python3-DL/3.6.9 | python3-DL/3.7.6 | python3-DL/3.8.5 | python3-DL/3.8.8 | python3-DL/3.8.0 | python3-DL/3.9.7 |
| ------------ | ---------------- | ---------------- | ---------------- |------------------| ---------------- | ---------------- |------------------|
| architecture | k80, p100, v100  | k80, p100, v100  | k80, p100, v100  | k80, p100, v100  | k80, p100, v100  | k80, p100, v100  | k80, p100, v100  |
| python       | 3.6.1            | 3.6.9            | 3.7.6            | 3.8.5            | 3.8.8            | 3.8.0            | 3.9.7            |
| cuda         | 9.0              | 10.0             | 10.1             | 10.1             | 11.0             | 11.1             | 11.1             |
| tensorflow   | 1.8              | 1.14             | 2.1              | 2.3.1            | 2.4.1            | 2.5.0            | 2.6.0            |
| torch        | 1.0.1            | 1.2.0            | 1.4.0            | 1.6.0            | 1.7.1            | 1.8.1            | 1.9.1            |
| keras        | 2.2.4            | 2.2.5            | 2.3.1            | 2.4.3            | 2.4.3            | NA               | NA               |
| scikit-learn | 0.20.0           | 0.21.3           | 0.22.1           | 0.23.2           | 0.24.1           | 0.24.2           | 1.0              |
| scikit-image | 0.14.2           | 0.15.0           | 0.16.2           | 0.17.2           | 0.19.3           | 0.18.1           | 0.19.3           |
| opencv       | 3.4.0            | 4.1.0            | 4.2.0            | 4.4.0            | 4.5.1            | 4.5.2            | 4.5.3            |
| pandas       | 0.24.0           | 0.25.1           | 1.0.1            | 1.1.4            | 1.2.3            | 1.2.5            | 1.3.3            |

De nombreux packages complémentaires enrichissent les principaux outils installés. La liste exhaustive des packages, une fois le module chargé, s’obtient par la commande : `pip list`

### Packages manquants

Si certains packages sont manquant dans l’installation proposée il est recommandé de demander leurs installations à l'équipe technique du CRIANN en adressant la requête à [support@criann.fr](mailto:support@criann.fr?subject=%5BCalcul%5D%20Demande%20d%27installation%20d%27un%20logiciel%20manquant%20sur%20MYRIA&body=Bonjour%2C%0A%0AJe%20souhaiterai%20voir%20avec%20vous%20pour%20l%27installation%20du%20logiciel%20suivant%20sur%20MYRIA.%0A)

Dans certains cas, il est possible d'installer les packages en local sur le home utilisateur à l’aide de la commande : `pip install <package> --user`

Dans ce cas, le répertoire local par défaut dans lequel le package sera installé dépend de la version de module chargé :

- python3-DL/3.6.9 : `~/.python-DL-3.6.9/site-packages`
- python3-DL/3.6.1 : `~/.python-DL-3.6.1/site-packages`
- python3-DL/3.7.6 : `~/.python-DL-3.7.6-2/site-packages`
- python3-DL/3.8.5 : `~/.python-DL-3.8.5/site-packages`
- python3-DL/3.8.8 : `~/.python-DL-3.8.8/site-packages`
- python3-DL/3.8.0 : `~/.python-DL-3.8.0/site-packages`
- python3-DL/3.9.7 : `~/.python-DL-3.9.7/site-packages`
  
## Utilisation

Pour la plupart des frameworks de Deep Learning l’utilisation des ressources GPUs est recommandée (voire obligatoire dans le cas de tensorflow).

Pour accéder aux ressources gpus il est obligatoire de tourner sur les partitions `gpu_k80`, `gpu_p100` ou `gpu_v100`.

**Attention** : ne pas oublier de préciser le nombre de GPUs affecter à votre calcul grâce à l’option de slurm : `--gres gpu:X` où `X` désigne le nombre de device GPU à charger.

Bien que calculant majoritairement sur GPUs, la plupart des frameworks font un usage multi-threadé de python. Il est donc pertinent d’affecter plusieurs cpus à une même tâche python. Pour cela il suffit d’utiliser l’option `--cpus-per-task` de slurm. Le choix du nombre de cpus par tâche se fait en proportion du nombre de GPUs utilisés et dépend de la partition ciblée :

- `gpu_k80` : jusqu’à 7 cpus par gpu
- `gpu_p100` : jusqu’à 14 cpus par gpu
- `gpu_v100` : jusqu’à 8 cpus par gpu

Sur Myria, le script slurm : `/soft/slurm/criann_modeles_scripts/job_tensorflow.sl` est un exemple pour un calcul tensorflow monogpu sur architecture p100.
