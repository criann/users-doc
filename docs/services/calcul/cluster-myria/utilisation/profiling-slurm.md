---
title: "Profiling sous Slurm"
---
# Profiling sous Slurm

**_Utilisation de la fonctionnalité de profiling de Slurm, accessible depuis la mise à jour de l'été 2020_**

!!! note "Profiling désactivé par défaut"
    Par défaut, les calculs tournent sans l'activation du profiling.

## Principe

Par défaut, Slurm indique et stocke la consommation mémoire d'un calcul. Celle-ci est consultable après la fin du calcul par la commande `sacct`. Quand le profiling est activé pour un calcul, Slurm collecte les données périodiquement et les stocke dans un fichier HDF5.

## Usage

Lors du lancement du calcul (`sbatch`), rajouter l'option `--profile=all`

Une fois le calcul terminé, demander la collecte des fichiers générés en exécutant la commande `sh5util -j $JOB_ID`, en remplaçant $JOB_ID par le numéro du calcul. Celle-ci génère un fichier (job_$JOB_ID.h5) au format HDF5 contenant les données récoltées, dans le dossier en cours.

## Les données

Sur Myria, seules les données liées aux tâches de calculs sont interrogeables (task). Vous pouvez suivre l'évolution de l'utilisation de la mémoire (RSS et VMSize).

Le système de fichiers GPFS n'est pas compatible avec le plugin de surveillance des lectures/écriture. De même le réseau Omni-Path n'est pas compatible avec le plugin de surveillance du réseau.  
Pour visualiser le contenu du fichier HDF5, installer le logiciel `HDFView` sur votre poste de travail.

## Pour aller plus loin

Pour plus d'informations, consulter la documentation Slurm : <https://slurm.schedmd.com/archive/slurm-19.05.7/hdf5_profile_user_guide.html>
