# Modules

**_Gestion d'environnement d'applications et de librairies_**

## Commandes

Taper `module help` pour l'ensemble des options de la commande module

- `module avail` : lister les modules disponibles
- `module avail nom_appli` : lister les modules disponibles pour l'application indiquée
- `module help nom_appli/version` : afficher l'aide en ligne (si présente) pour l'application indiquée
- `module load nom_appli/version` : charger l'environnement du module indiqué
- `module unload nom_appli/version` : décharger l'environnement du module indiqué
- `module list` : lister les environnements chargés
- `module purge` : décharger tous les environnements chargés au préalable

## Cas des librairies

**Le chargement d'un module de librairie permet de compiler sans appliquer d'options `-I ...` ni `-L ...` relatives à la librairie en question dans un Makefile, si le compilateur Intel est utilisé**. Le même Makefile peut alors être employé sur plusieurs centres de calcul ayant mis en place un module pour la librairie en question.

Pour la structure générale de fichiers Makefile, des exemples figurent dans le répertoire `/soft/makefiles`.

## Exemples

**_Application Castem_**

```bash
login@Myria-1:~ module avail castem

------------------------------------------------- /soft/Modules/Myria_modules/cfd_fem_meshing --------------------------------------
castem/2013  castem/2016

login@Myria-1:~: module load castem/2016
load castem/2016 environment
```

**_Application Paraview_**

```bash
login@Myria-1:~ module help paraview/5.2.0_Serial
----------- Module Specific Help for 'paraview/5.2.0_Serial' ---------------------------
For visualization jobs, a documentation is available : http://www-tech.criann.fr/calcul/tech/myria-doc/visu/
```

**_Librairie FFTW_**

```bash
login@Myria-1:~ module avail fftw
------------------------------------------------------ /soft/Modules/Myria_modules/libraries ----------------------------------------------
fftw/3.3.5/impi  fftw/3.3.5/serial  fftw/3.3.5/serial-threaded

login@Myria-1:~: module whatis fftw/3.3.5/impi
fftw/3.3.5/impi environment
fftw/3.3.5/impi : fftw 3.3.5 / Intel MPI / ifort - icc - icpc

login@Myria-1:~: module load fftw/3.3.5/impi
load fftw/3.3.5/impi environment
```
