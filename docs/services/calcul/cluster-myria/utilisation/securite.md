---
title: "Sécurité"
---

# Sécurité : comment changer ses moyens de connexion

## Changement du mot de passe

- Se connecter sur une des frontales de connexion
- Taper la commande `passwd` et répondre aux questions en indiquant le mot de passe en cours puis 2 fois le nouveau mot de passe.
Cela modifiera le mot de passe de toutes les frontales.

## Changer la clé SSH de connexion

Si vous avez configuré une clé SSH pour vous connecter sur Myria, il faut supprimer l'ancienne clé, régénérer la passphrase qui protège votre clé privée et rajouter la nouvelle publique. Si vous n'avez pas configuré de clé SSH de connexion, vérifiez tout de même le contenu du fichier `~/.ssh/authorized_keys` comme indiqué plus bas.

- Sur votre poste de travail :
    - régénérer les paires de fichiers (clé privée / clé publique) en utilisant la commande `ssh-keygen` (sous Linux/MacOS) ou via l'application `putty-gen` (sous Windows).  
      Pour des raisons évidentes de sécurité, privilégiez une connexion avec une passphrase NON vide avec, à la rigueur, l'utilisation d'un ssh-agent.
- Sur Myria :
    - faire du ménage dans les clés autorisées dans le fichier `~/.ssh/authorized_keys`  
      Seule la ligne portant le commentaire `login@machine` (où `machine` peut être `altair-adm`, `antares` ou `mytch`) en fin de ligne doit être conservée. Elle vous permet de se connecter au sein du cluster.  
      Pour les autres lignes, ne conserver que l'indispensable : les connexions par mot de passe resteront possibles.
    - ajouter la nouvelle clé publique dans le fichier `~/.ssh/authorized_keys`

## Cas des connexions entre les centres de calcul

Si vous avez mis en place des clés SSH pour vous connecter entre les centres de calcul, il faut également supprimer les paires de clés publiques et privées sur chaque centre, les re-générer et rediffuser les clés publiques.  
Même remarque que précédemment : privilégiez une connexion avec une passphrase NON vide...

!!! warning "Attention, très important"
    votre clé privée (fichier `id_rsa`) ne doit jamais être recopiée sur un autre serveur... Elle doit rester uniquement sur votre poste de travail.

## Recommandations de l'IDRIS

En complément, voici les recommandations du centre national IDRIS :

Pour plus de sécurité, nous vous demandons quatre choses :

- prendre toutes les précautions sur votre poste de travail pour protéger votre clé privée (phrase de passe solide, droits d'accès restrictifs),
- ne pas copier votre clé privée sur les serveurs de l'IDRIS,
- générer des clés RSA d'au moins 4096 bits, ou d'utiliser des algorithmes de cryptographie sur les courbes elliptiques (ECDSA, ed25519)
- vérifier que les empreintes (fingerprint) des clés publiques des serveurs SSH de l'IDRIS sur lesquels vous vous connectez sont bien référencées sur cette liste
- restreindre ces clés à la seule utilisation des machines que vous avez déclarées dans les filtres de l'IDRIS. Pour cela, vous devez éditer le fichier `authorized_keys` de votre machine locale et ajouter au début de chaque ligne contenant une clé générée sur une des machines de l'IDRIS, la chaîne de caractères suivante :

    - `from="votre_ip"` ou éventuellement ;
    - `from="machine.domaine_local.fr"` ou ;
    - `from="*.machine.domaine_local.fr"`.

    ```bash
    $ cat ~/.ssh/authorized_keys
    from="machine.domaine_local.fr" ssh-rsa AAAAB3NzaC1yc2EA........... login@host
    ```