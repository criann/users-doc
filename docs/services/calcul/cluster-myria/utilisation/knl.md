# KNL

## Architecture

Le KNL est un processeur à grand nombre de cœurs (64 à 72) de fréquence modérée (1,3 à 1,5 GHz). Un mode de SMT (Simultaneous Multi Threading) permet à chaque cœur d'exécuter quatre processus si l'utilisateur le souhaite.

Cette architecture est adaptée à des applications parallèles (OpenMP ou multithreadées, MPI ou MPI+OpenMP notamment), qui accélèrent jusqu'à au moins quelques dizaines de processus. L'hybridation MPI+OpenMP peut aider à la performance par rapport à l'approche pure MPI.

### Mémoire

Le processeur KNL possède une zone de mémoire rapide (MCDRAM) embarquée, d'une taille de 16 Go.

Un serveur KNL peut être configuré selon trois modes différents pour l'utilisation de cette mémoire par une application.

- **Mode _cache_**

    Dans ce mode, la mémoire MCDRAM est utilisée par le système comme un cache de troisième niveau ("cache L3") de grande taille (16 Go) : les données provenant de la RAM (DDR4) d'un serveur KNL sont périodiquement stockées dans cette zone de mémoire rapide et proche du processeur.

- **Mode _flat_**

    Dans ce mode, la mémoire MCDRAM est un espace de mémoire adressable, dans lequel une application peut allouer tout ou partie de ses données (les autres restant allouées dans la RAM du serveur). Ces allocations peuvent être spécifiées dans le programme ou par des commandes d'exécution (ceci est précisé dans le paragraphe "Exploitation de la mémoire rapide MCDRAM").

- **Mode _hybride_**

    Dans le mode hybride, le quart ou la moitié (4 ou 8 Go) de la MCDRAM est un cache L3, le reste (8 ou 12 Go) est un espace de mémoire adressable.

![KNL](../../../../assets/images/knl_mem.gif)

### Modes de clustering

Les principaux modes de clustering, i.e. de d'interconnexion des cœurs au sein d'un KNL sont les suivants. La notion d'entité NUMA (Non Uniform Memory Access), évoquée ci-dessous, est une architecture dans laquelle les cœurs accèdent plus rapidement à certaines zones de la mémoire qu'à d'autres zones.

- Mode quadrant

    Dans le mode quadrant, les cœurs du KNL sont organisés en quatre groupes, chacun localisé à proximité d'un contrôleur de mémoire. Un KNL en mode quadrant est vu par le système comme une unique entité NUMA.

- Modes SNC

    Dans les modes SNC (Sub Numa Clustering), le KNL est subdivisé en deux (mode SNC-2) ou en quatre (mode SNC-4) entités NUMA.

### Configuration sur Myria

Myria comprend dix nœuds de calcul dotés chacun d'un KNL de modèle 7210, i.e. à 64 cœurs (4 threads par cœurs), 1,3 GHz de fréquence CPU avec 96 Go de RAM DDR4 à 2133 MHz par nœud.

Ces machines (de même que les machines à processeur généraliste Broadwell de Myria) sont interconnectées par un réseau Omni-Path à faible latence et haut débit (100 Gbit/s). Le contrôleur Omni-Path est intégré à la puce KNL.

Ces dix nœuds KNL sont configurés (statiquement) en mode de clustering quadrant et en mode de mémoire cache pour la MCDRAM embarquée de 16 Go.

Cette configuration peut être modifiée sur plusieurs nœuds (par exemple, pour tester le mode flat pour la MCDRAM) sur demande adressée à [support@criann.fr](mailto:support@criann.fr).

## Exploitation de la mémoire rapide MCDRAM

Dans le cas du mode cache pour la mémoire MCDRAM, c'est le système d'exploitation qui exploite cette mémoire comme un cache L3 de 16 Go.

Dans le cas du mode flat pour la mémoire MCDRAM, l'allocation de données dans cette mémoire peut être spécifiée par les deux biais suivants (lors de l'exécution avec numactl ou au niveau du programme de l'application).

### Commande numactl

Lorsque la quantité de mémoire utilisée par une application ne dépasse pas 16 Go, la commande suivante est utilisable pour spécifier à l'exécution une allocation de la totalité des données dans la MCDRAM :

- dans le cas du mode de clustering quadrant : `$runcmd numactl --membind=1 ./a.out`
- dans le cas du mode de clustering SNC-2 : `$runcmd numactl --membind=2,3 ./a.out`

Si une application utilise plus de 16 Go de mémoire, dans le cas d'un KNL en mode de clustering quadrant, l'option `--preferred` de `numatcl` est disponible pour allouer préférentiellement 16 Go de données dans la MCDRAM (le reste étant alloué dans la RAM DDR4 du serveur) :

- dans le cas du mode de clustering quadrant : `$runcmd numactl --preferred=1 ./a.out`

La variable `$runcmd` précédente désigne une commande de type `mpirun` ou `srun` pour l'exécution des codes utilisant la librairie MPI (voir le fichier `job_KNL.sl` présent dans `/soft/slurm/criann_modeles_scripts/` sur Myria).

### Librairie memkind

Alternativement, l'allocation en MCDRAM peut être spécifiée pour telle ou telle donnée au niveau du programme d'une application.

La librairie memkind fournit en effet des fonctions d'allocation en mémoire rapide. Elle s'utilise différemment en FORTRAN et en C.

#### Cas du FORTRAN

La directive de compilation `!DEC$ ATTRIBUTE FASTMEM :: VAR` peut être utilisée avant une instruction d'allocation pour imposer qu'elle soit faite dans la MCDRAM.

Par exemple :

```fortran linenums="1" hl_lines="5"
REAL, ALLOCATABLE :: A(:), B(:)

! FASTMEM attribute

!DEC$ ATTRIBUTES FASTMEM :: A

! A is allocated in MCDRAM

ALLOCATE (A(1:1024))

! B is allocated in DDR4

ALLOCATE (B(1:1024))
```

#### Cas du C

Des fonctions d'allocation en mémoire rapide sont disponibles.

Par exemple :

```c linenums="1"
#include <hbwmalloc.h> /* hbwmalloc interface */

/* ... */

const int n = 1<<10;

double* A = (double*) hbw_malloc (sizeof(double)*n); /* Allocation to HBM (high bandwidth memory, MCDRAM) */

/* ... */

hbw_free (A); /* Deallocate with hbw_free */
```

#### Compilation avec `memkind`

Une application utilisant les directives (FORTRAN) ou fonctions (C) précédentes de memkind peut se compiler sur Myria avec le compilateur Intel : appliquer `-lmemkind` en option d'édition de lien ou taper `module load memkind` avant de compiler.

## Options de compilation

L'option d'optimisation pour l'architecture KNL est `-xMIC-AVX512`.

Les jeux suivants d'options d'optimisation peuvent être employés :

- pour un exécutable optimisé pour KNL (mais non exécutable sur processeur généraliste Broadwell : `-O3 -xMIC-AVX512`
- pour un exécutable optimisé KNL et Broadwell : `-O3 -axCORE-AVX2,MIC-AVX512`

## Soumission des calculs

Sur Myria, le répertoire `/soft/slurm/criann_modeles_scripts` comprend le modèle générique `job_KNL.sl` pour soumettre un calcul sur KNL avec Slurm.

Le modèle `job_KNL_quad_cache.sl` correspond aux options de configuration par défaut : modes quadrant (clustering) et cache (mémoire MCDRAM).

La partition `knl` doit être indiquée et la directive `--constraint` permet de spécifier les modes de clustering et de mémoire MCDRAM. Par exemple pour les modes quadrant et cache :

```
#SBATCH --constraint quad,cache
```

ou pour les modes quadrant et flat (configuration à demander à [support@criann.fr](mailtpo:support@criann.fr)) :

```
#SBATCH --constraint quad,flat
```

## Bonnes pratiques

Le projet PRACE a publié un guide de bonnes pratiques pour la performance sur KNL : 
<https://prace-ri.eu/training-support/best-practice-guides/best-practice-guide-knights-landing/> 
