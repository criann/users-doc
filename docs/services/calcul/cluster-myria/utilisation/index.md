---
title: "Guide d'utilisation"
---
# Guide d'utilisation du calculateur Myria

Si vous débutez dans l'environnement HPC, nous vous recommandons de lire la version détaillée au [:fontawesome-solid-file-pdf:{: .red}format PDF](../documents/utilisation-myria.pdf). Sinon, merci de lire au moins les deux premiers paragraphes de la documentation synthétique avant de contacter le support.

## Connexion, environnement et espaces de travail

La connexion s'effectue en SSH vers les frontales regroupées sous le nom `myria.criann.fr`.

**Syntaxe en ligne de commande** : `ssh monlogin@myria.criann.fr` (en remplaçant `monlogin` par votre identifiant).
Les environnements Linux et Mac intègrent nativement le protocole SSH via un terminal.
Si vous êtes sous environnement Windows, nous vous recommandons d'utiliser le logiciel [MobaXTerm](https://mobaxterm.mobatek.net/) qui vous apportera un environnement de travail complet basé du protocole ssh (export d’écran, transfert de fichiers).

Lors de votre première connexion, le changement de mot de passe vous est imposé. Lisez soigneusement ce qui vous est demandé : `(current) Password` correspond au mot de passe actuel et non au nouveau mot de passe souhaité...

Les personnalisations (variables d'environnement, alias) se font avec un fichier `~/.bash_profile` (et non `~/.bashrc`) à créer.

L'utilisateur a un espace de travail personnel dans `/home`.

Par défaut un quota disque utilisateur est positionné à 50 Go dans `/home` ; la commande `mmlsquota gpfs1:home` fournit le quota et la place que l'utilisateur occupe dans la partition `/home`.

Nous vous encourageons à calculer dans les dossiers de scratch temporaires (`/dlocal/run/jobid`) créés par l'outil de batch Slurm pour chaque calcul. Un quota de 10 millions de fichiers est appliqué sur l'arborescence `/dlocal` ; la commande `mmlsquota gpfs1:dlocal` fournit le quota et le nombre de fichiers vous appartenant dans l'arborescence `/dlocal/run`.

Si vous estimez que ces limites (quotas) sont trop contraignantes pour vous, n'hésitez pas à ouvrir un ticket auprès du support. Celles-ci peuvent être augmentées sur demande justifiée.

Pour plus d'informations sur cet espace de travail et sur les commandes recommandées pour gérer vos fichiers, consulter [cette page](gestion-donnees.md).

## Transferts de données

Pour le transfert de quantités significatives de données entre Myria et l'extérieur, le serveur **myria-transfert.criann.fr** doit être privilégié car il est connecté au réseau SYVIK par une interface 40 Gbit/s.

Cette machine donne accès au même répertoire `/home` que les frontales. Les identifiants de connexion (ssh, scp, sftp) sur **myria-transfert.criann.fr** sont les mêmes que sur **myria.criann.fr**.

Les transferts entre Myria et les ressources de l'IDRIS ne sont autorisés que par **myria-transfert.criann.fr**.

## Firewall

Si votre structure a un firewall limitant les flux en sortie de votre site, voici les ports à ouvrir :

- **Connexions SSH (port 22) vers les 4 frontales derrière le nom myria.criann.fr**
    - IPv4 : 195.221.27.66 (myria-front1.criann.fr) à 195.221.27.69 (myria-front4.criann.fr)
    - IPv6 : 2001:660:7401:273::66 (myria-front1.criann.fr) à 2001:660:7401:273::69 (myria-front4.criann.fr)
- **Connexions SSH (port 22) vers la frontale de transfert**
    - IPv4 : 195.221.27.70 (myria-transfert.criann.fr)
    - IPv6 : 2001:660:7401:273::70 (myria-transfert.criann.fr)

Concernant les sessions de visualisation à distance, pour connaître les IP et les ports à ouvrir, merci de contacter le support par mail à support@criann.fr

## Logiciels disponibles

 La plupart des modèles de script de soumission pour les logiciels suivants sont disponibles dans le répertoire `/soft/slurm/criann_modeles_scripts`.

Dans le cas contraire, contacter [support@criann.fr](mailto:support@criann.fr) (de même que pour demander un nouveau logiciel).

Les environnements d'applications et de librairies sont accessibles par des modules (voir les commandes `module avail`, `module help` et la documentation générale sur les modules).

**_Logiciels commerciaux_** (licence à prendre par l'utilisateur)

- **CFD/Mécanique** : ANSYS Fluent, StarCCM+, FINE/Marine, Hyperworks

**_Logiciels libres_**

- **Mécanique des fluides** :
    - FDS 5.5.0 (incendies)
    - Telemac v7p1r1, v8p1r1, v8p2r0 et v8p3r1
    - Code_Saturne 5.0.7 et 6.1.1
    - OpenFOAM 2.3.0, 2.4.0, 3.0.1, 3.0.x, 4.x-version-4.0, 4.x-version-4.1, 5.x-version-5.0, 5.x-20171030, 6-version-6, 6-20190304, 7-version-7, 8-version-8, 10-version-10, 1606+, 1612+, 1706, 1712, 1806, 1812, 1906, 1912, 2006, 2012, 2106, 2112 et 2206
    - Foam-extend 3.2 et 4.0
- **Mécanique des structures** :
    - Castem 2016, 2017, 2019 et 2021
    - Code_Aster 11.6.0 séquentiel, 13.4.0 séquentiel, 14.2.0_testing ("version développement stabilisée") séquentiel et parallèle (MPI), 14.6.0 séquentiel
    - Salome-Meca 2017.0.2
    - LMGC90/user_2019_rc1
- **Chimie** :
    - GAMESS 2013 et 2016
    - Dalton 2018
    - Psi4
    - Cfour v1
    - Open Babel 3.0.0
- **Dynamique moléculaire** :
    - GROMACS 5.1.4, 2019.2, 2020.4, 2021.1 et 2022.1 (patch Plumed ou non)
    - NAMD 2.9 (patch Plumed), 2.12 et 2.13 (patch Plumed ou non)
    - DL_POLY_CLASSIC 1.9
    - LAMMPS 7Aug19 et 7Jan2022
    - OpenMolcas 19.11
    - Quantum Espresso 6.5
- **Climat** : WRF 3.7.1, 3.9, 3.9.1 et 4.3
- **Océan** : Croco v1.1
- **Mathématiques** :
    - Octave 3.8.2 et 4.2.1
    - R 3.3.2, 3.5.1, 3.5.2, 4.0.2, 4.0.3, 4.0.5, 4.1.1 et 4.1.2
    - Scilab 5.5.2 et 6.0.1
- **IA – Deep Learning** : une [page est consacrée à la logithèque installée](ia-deep-learning.md)

**_Logiciels sous licence académique_**

- **Chimie quantique** :
    - GAUSSIAN 03
    - Shrödinger JAGUAR 8.0 et 9.4
- **Dynamique moléculaire** : Shrödinger Desmond 2016.3

**_Logiciels pour la visualisation ou les pré-/post-traitements_**

- Gmsh 2.16.0 et 4.5.1
- Neper 3.0.1
- Triangle 1.6
- ParaView 4.0.1, 5.2.0, 5.4.1, 5.6.0, 5.7.0 et 5.9.0
- Salome 7.8.0 et 8.5.0
- Visit 2.12.1 et 2.12.3
- Blender 2.76b et 2.79b
- ffmpeg 4.1
- Helix-OS
- XCrysDen 1.6.2
- VMD 1.9.3

## Compilation

Les environnements du compilateur et de la librairie MPI d'Intel, en version 2017, sont activés par défaut (taper `module list`).

Les codes MPI se compilent au moyen des commandes `mpiifort` (FORTRAN), `mpiicc` (C, 2 "i" dans le nom de cette commande), `mpiicpc` (C++).

L'option d'optimisation pour l'architecture des processeurs généralistes (Broadwell) est `-xCORE-AVX2` (pour une compilation sur une frontale, ou en batch dans une partition autre que knl, l'option `-xHost` est équivalente).

L'option d'optimisation pour l'architecture des processeurs manycores Xeon Phi KNL est `-xMIC-AVX512`.

La génération d'un exécutable optimisé pour les deux architectures Broadwell et KNL est possible, avec l'option `-axCORE-AVX2,MIC-AVX512`.

**_Exemples de jeux d'options d'optimisation pour Broadwell, par ordre croissant d'agressivité_**

- `-O2 -xCORE-AVX2`
- `-O3 -xCORE-AVX2 -fp-model precise`
- `-O3 -xCORE-AVX2`

**_Exemple de jeu d'options d'optimisation pour KNL_**

- `-O3 -xMIC-AVX512`

**_Exemple de jeu d'options de débogage_**

- `-O2 -g -traceback -check all`

Des exemples de fichiers Makefile sont fournis dans `/soft/makefiles`.

L'**environnement de [modules](modules.md)** facilite la compilation d'applications liées aux librairies disponibles.

## Environnement de soumission (Slurm)

Le répertoire `/soft/slurm/criann_modeles_scripts` contient les **scripts de soumission** génériques (code séquentiel, OpenMP, MPI, hybride, KNL, GPU) ou relatifs à une application spécifique.

### Mémoire

Il est conseillé d'employer la directive `#SBATCH --mem` de Slurm, spécifiant la demande mémoire par nœud de calcul (plutôt que la mémoire par cœur réservé, `--mem-per-cpu`).

**Si un calcul parallèle a besoin d'une quantité de mémoire par tâche MPI supérieure à 4300MB, les nœuds de calcul standard de Myria doivent être dépeuplés par l'ajout de la directive** `#SBATCH --ntasks-per-node` dans le script de soumission.

Par exemple, les paramètres suivants demandent un total de 140 tâches MPI avec, par nœud de calcul (serveur) standard, 14 tâches MPI et 120000&nbsp;MB de mémoire (ces serveurs ayant 28 cœurs chacun, la moitié des unités de calcul seront exploitées mais toute la RAM sera disponible) :

```
# Partition (submission class)
#SBATCH --partition 2tcourt

# MPI tasks number
#SBATCH --ntasks 140

# MPI tasks per compute node
#SBATCH --ntasks-per-node 14

# Maximum memory per compute node (MB)
#SBATCH --mem 120000
```

### Commandes

Ce tableau fournit les commandes utiles pour la soumission des travaux.

| Action                                                                                      | Commande                                  |
| ------------------------------------------------------------------------------------------- | ----------------------------------------- |
| Charge du calculateur                                                                       | `slurmtop -f -`                           |
| Caractéristiques des partitions (classes)                                                   | `sinfo`                                   |
| Soumettre un travail                                                                        | `sbatch script_soumission.sl`             |
| Soumettre un travail en "hold" (attente)                                                    | `sbatch -H script_soumission.sl`          |
| Libérer un travail en "hold" (attente)                                                      | `scontrol release job_id`                 |
| Lister l'ensemble des travaux                                                               | `squeue`                                  |
| Lister ses propres travaux                                                                  | `squeue -u login`                         |
| Affichage des caractéristiques d'un travail                                                 | `scontrol show job job_id`                |
| Prévision d'horaire de passage d'un travail en file d'attente                               | `squeue --start --job job_id`             |
| Vérification de la syntaxe et prévision d'horaire de passage d'un travail sans le soumettre | `sbatch --test-only script_soumission.sl` |
| Prévision d'horaire de passage de ses propres travaux                                       | `squeue -u login --start`                 |
| Tuer un travail                                                                             | `scancel job_id`                          |

### Variables d'environnement

Les variables utilitaires suivantes (liste non exhaustive) peuvent être exploitées dans les commandes utilisateurs (Shell) d'un script de soumission.

| Nom de variable     | Valeur                                                                                                                                                                       |
| ------------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `$SLURM_JOB_ID`     | Identification du travail (exemple : 64549)                                                                                                                                  |
| `$SLURM_JOB_NAME`   | Nom du travail (spécifié par `#SBATCH -J`)                                                                                                                                   |
| `$SLURM_SUBMIT_DIR` | Nom du répertoire initial (dans lequel la commande sbatch a été lancée)                                                                                                      |
| `$SLURM_NTASKS`     | Nombre de processus MPI du travail                                                                                                                                           |
| `$LOCAL_WORK_DIR`   | Nom du répertoire de scratch temporaire fondé sur le numéro du calcul : `/dlocal/run/$SLURM_JOB_ID`. <br />Cette arborescence est supprimée 45 jours après la fin du calcul. |

### Les partitions (classes de soumission)

Les partitions utilisent les 368 nœuds bi-sockets Broadwell (10304 cœurs), les 10 nœuds Xeon Phi KNL (640 cœurs) et le nœud SMP (256 cœurs).

Le nombre de nœuds de calcul, auxquels les travaux des différentes partitions accèdent, sont donnés ci-dessous à titre indicatif. Les administrateurs ajustent parfois légèrement la taille de ces «fenêtres de tir» en fonction de la charge observée. En revanche, les limitations par calcul restent en général constantes. Dans tous les cas, ces limites ne sont pas modifiées sans qu’une annonce soit faite à la liste de diffusion des utilisateurs.

**Remarque** : le caractère reproductible des performances d’une application sur le calculateur peut être renforcé par l'application dans le script de soumission de la directive `#SBATCH --exclusive` qui impose l’exécution du travail soumis sur un ensemble de nœuds non partagés. Cette directive est **conseillée pour les travaux de production MPI multi-nœuds, recommandée dans le cas de tests de performance**.

Pour des calculs de production ou de développement scientifique, dans le cas particulier d'applications séquentielles il est préférable de ne pas appliquer cette directive (pour ne pas monopoliser un nœud de calcul).

La partition est spécifiée par le script de soumission avec la directive `#SBATCH --partition` (ou dans la ligne de commande : `sbatch --partition 2tcourt script_soumission.sl`)

**_Travaux de debug_**

Cette partition regroupe tous les serveurs équipés du processeur Xeon Broadwell.

| Partition | Durée maximale | Nœuds disponibles | Limites par calcul                                          | Conseils                                                                      |
| --------- | -------------- | ----------------- | ----------------------------------------------------------- | ----------------------------------------------------------------------------- |
| debug     | 0 h 30         | 363 nœuds         | 5 nœuds (140 cœurs et 585 Go / 28 cœurs et 1 To de mémoire) | L'un des 363 serveurs associés à cette partition est équipé d'1 To de mémoire |

**_Travaux sur architecture standard (Broadwell)_**

Les partitions sont définies en fonction des quantités de ressource demandées.

| Partition    | Durée maximale | Nœuds disponibles | Limites par calcul                                                                                                                 | Conseils                                                     |
| ------------ | -------------- | ----------------- | ---------------------------------------------------------------------------------------------------------------------------------- | ------------------------------------------------------------ |
| 2tcourt      | 12 h           | 332 nœuds         | 75 nœuds en semaine (2100 cœurs et 9600 Go de mémoire) <br />150 nœuds pendant le week-end (*) (4200 cœurs et 19200 Go de mémoire) | `#SBATCH --mem 120000MB` pour MPI ou OpenMP nœud(s) plein(s) |
| tcourt       | 24 h           | 322 nœuds         | 75 nœuds (2100 cœurs et 9600 Go de mémoire)                                                                                        | `#SBATCH --mem 120000MB` pour MPI ou OpenMP nœud(s) plein(s) |
| court        | 48 h           | 100 nœuds         | 20 nœuds (560 cœurs et 2560 Go de mémoire)                                                                                         | `#SBATCH --mem 120000MB` pour MPI ou OpenMP nœud(s) plein(s) |
| long         | 100 h          | 50 nœuds          | 10 nœuds (280 cœurs et 1280 Go de mémoire)                                                                                         | `#SBATCH --mem 120000MB` pour MPI ou OpenMP nœud(s) plein(s) |
| tlong        | 300 h          | 15 nœuds          | 2 nœuds (56 cœurs et 256 Go de mémoire)                                                                                            | `#SBATCH --mem 120000MB` pour MPI ou OpenMP nœud(s) plein(s) |
| tcourt_intra | 24 h           | 134 nœuds         | 1 nœud (28 cœurs et 128 Go de mémoire)                                                                                             | `#SBATCH --mem 120000MB` pour MPI ou OpenMP nœud plein       |

(*) l'horaire du week-end commence le vendredi soir à 17h et se termine le dimanche soir à 20h. Pendant cette période, le total des cœurs utilisé par un utilisateur peut atteindre 4200 cœurs, autorisant ainsi un calcul dans la partition 2tcourt jusqu'à 4200 cœurs.

**_Travaux sur architectures spécifiques_**

Les partitions correspondent au type de l'architecture ciblée.

| Partition | Durée maximale | Nœuds disponibles                                              | Limites par calcul                        | Conseils                                                                   |
| --------- | -------------- | -------------------------------------------------------------- | ----------------------------------------- | -------------------------------------------------------------------------- |
| disque    | 300 h          | 13 nœuds (Broadwell) (dont 12 à 128 Go et 1 à 1 To de mémoire) | 28 cœurs et 1 To de mémoire<br />(`#SBATCH --mem 1016GB`)               | Scratch disque local (non partagé en réseau) : `$LOCAL_SCRATCH_DIR`        |
| smplarge  | 3 h            | Totalité du serveur SMP (Haswell)                              | 256 cœurs et 3,99 To de mémoire           | `#SBATCH --mem 4090GB` (disponible)                                        |
| smplong   | 72 h           | Moitié du serveur SMP (Haswell)                                | 128 cœurs et 2 To de mémoire              | `#SBATCH --exclusive` interdite <br /> `#SBATCH --mem 2045GB` (disponible) |
| knl       | 100 h          | 10 nœuds (KNL)                                                 | 4 nœuds (256 cœurs et 384 Go de mémoire)  | `#SBATCH --mem 89000MB` pour MPI ou OpenMP nœud(s) plein(s)                |
| gpu_k80   | 48 h           | 9 nœuds (Broadwell) (avec GPUs Kepler K80)                     | 8 nœuds (224 cœurs et 1024 Go de mémoire) |                                                                            |
| gpu_p100  | 48 h           | 9 nœuds (Broadwell) (avec GPUs Pascal P100)                    | 8 nœuds (224 cœurs et 1024 Go de mémoire) |                                                                            |
| gpu_v100  | 48 h           | 5 nœuds (SkyLake) (avec GPUs Volta V100)                       | 5 nœuds (160 cœurs et 900 Go de mémoire)  |                                                                            |
| gpu_all   | 48 h           | 18 nœuds (Broadwell) (avec GPUs K80 ou P100)                   | 1 nœud (28 cœurs et 128 Go de mémoire)    |                                                                            |
| gpu_court | 4 h            | 4 nœuds (Broadwell) (avec GPUs K80)                            | 1 nœud (28 cœurs et 128 Go de mémoire)    |                                                                            |
| visu      | 6 h            | 2 nœuds (Broadwell) à 256 Go de RAM                            | 8 cœurs et 128 Go de mémoire              |                                                                            |
| visu2     | 6 h            | 2 nœuds (Broadwell) à 128 Go de RAM                            | 8 cœurs et 128 Go de mémoire              |                                                                            |

La partition **disque** doit être spécifiée pour des applications de chimie intensives en débit d'écriture.
Voir les modèles de fichiers de soumission dans `/soft/slurm/criann_modeles_scripts` : 

- `job_Gamess2013.sl`
- `job_Gamess2016.sl`
- `job_Jaguar-8.0.sl`
- `job_Jaguar-9.4.sl`
- `job_Gaussian03.sl`

### GPGPU

Une description des modalités de [soumission de travaux sur GPU](gpgpu.md) est disponible.

Un paragraphe concerne les utilisateurs devant compiler ou exécuter des applications multi-GPU (pour le cas CUDA aware MPI) sur l'[architecture V100-SXM2](gpgpu.md#architecture-v100-sxm2) de Myria.

### KNL

Les serveurs dotés d'un processeur Xeon Phi KNL ont plusieurs configurations (modes de "clustering" et de mémoire rapide embarquée).

La [page de documentation KNL](knl.md) décrit ces configurations et les directives de Slurm permettant de les spécifier pour les travaux.

### Visualisation à distance

Une [documentation spécifique pour les travaux de visualisation](visu.md) est disponible.

### Gestion des signaux envoyés par Slurm

Une [documentation spécifique pour attraper et traiter les signaux envoyés par Slurm](signaux-envoyes-par-slurm.md) est disponible.

### Compléments

La [documentation détaillée au format PDF](../documents/utilisation-myria.pdf) (paragraphes "**Bonnes pratiques de soumission**" et "**Exécution des travaux : aspects avancés**") fournit des informations utiles :

- Rapport de **consommation** de ressources (**mémoire** notamment) par calcul
- **Travaux multi-étapes** (pré-traitement, solveur et post-traitement par exemple) et **dépendances entre travaux**
- Prise en compte de la topologie du réseau pour la performance des communications MPI
- Placement optimal des processus parallèles d'un travail pour la performance (dans le cas d'utilisation de nœuds de calcul en mode dépeuplé)

## Suivi de la consommation horaire

Vous pouvez suivre la consommation horaire cpu.h de vos projets scientifiques sur le portail : <https://calcul-myria.criann.fr> .

En complément, depuis octobre 2020, à partir des frontales, la commande `maconso` affiche votre consommation ainsi que celle du projet. Si vous êtes responsable de projet ou responsable adjoint `maconso -u` indique le détail pour tous les comptes. 

Ces informations sont également accessibles au format JSON dans le fichier `~/.acct_myria.json`.

Les consommations sur le portail et en ligne de commande sont actualisées toutes les nuits.