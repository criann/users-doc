---
title: Jobs hétérogènes
---
# Gestion des calculs hétérogènes dans Slurm

**Depuis la version 17.11, Slurm a la capacité de gérer des calculs demandant des ressources hétérogènes. Par exemple une quantité  de mémoire différente sur certains cœurs ou bien la capacité de demander des calculs simultanés sur des partitions différentes pour effectuer un couplage.**

Cas de figure : le couplage entre 2 codes, code de visualisation, lancement en mode client-serveur (slave/master)...

# Soumettre des calculs

Lancer un calcul hétérogène constitué de 2 calculs tournant en même temps, sur des ressources différentes, revient à décrire ces 2 calculs dans le même script de soumission.

```bash linenums="1"
#!/bin/bash

### Partie commune
#SBATCH --time 0:10:00
#SBATCH --error=job_heterogene.%J.err
#SBATCH --output=job_heterogene.%J.out
### Ressources pour le premier calcul
#SBATCH -n 1 --mem-per-cpu 2000  --exclusive -p debug --time=0:10:00
#SBATCH packjob
### Ressources pour le deuxieme calcul
#SBATCH -n 2 --mem-per-cpu 3000 -p knl --time=0:20:00

# Recopie des fichiers
cp fichiers_entree $LOCAL_WORK_DIR
cd $LOCAL_WORK_DIR


# lancement des codes sur le pack-group associe
srun --pack-group=0 code1 >> sortie_code1.txt
srun --pack-group=1 code2 >> sortie_code2.txt

```

La soumission est identique à un calcul normal : `sbatch script.sl`

Autre solution, en ligne de commande :

```shell
# Job sur 3 cœurs (2 dans la partition `debug` et 1 dans la partition `knl`)
$ srun --label -n2 -pdebug : -n1 -pknl hostname
srun: job 8004622 queued and waiting for resources
srun: job 8004622 has been allocated resources
1: my010
0: my010
2: my362
```

## Suivi et arrêt d'un calcul

L'affichage des calculs dans `squeue` change : au niveau de slurm, cela correspond à 1 calcul contenant des sous-calculs. Ils apparaissent donc sous le même numéro avec "+num" correspondant au packjob.


```shell
$ squeue -u monlogin
             JOBID PARTITION     NAME     USER    ST       TIME  NODES NODELIST(REASON)
         8003734+0     debug heteroge    monlogin PD       0:00      1 (None)
         8003734+1       knl heteroge    monlogin PD       0:00      1 (None)
```
Pour tuer le calcul avec ses sous-calculs, il suffit d'utiliser `scancel` sur le chiffre du calcul (dans l'exemple ci-dessus : `scancel 8003734`)

Pour tuer le sous-calcul `+0` uniquement, il suffit de spécifier `scancel 8003734+0`.

!!! note "Subtilité"
    Si le calcul est en état `pending`, alors il n'est pas possible d'annuler les sous-calculs, on peut uniquement annuler le calcul et ses sous-calculs.


## Documentation Slurm
Pour plus d'information, consulter la documentation de Slurm en prenant soin de choisir la version identique à celle de Slurm sur Myria. En novembre 2020, Myria est sous Slurm 19.05.7.

Documentation : <https://slurm.schedmd.com/archive/slurm-19.05.7/heterogeneous_jobs.html>
