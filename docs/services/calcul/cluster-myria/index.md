---
title: Introduction
---
# Cluster MYRIA

Myria est une solution ATOS BULL dotée de 11304 cœurs de calcul, d'une puissance de 419 TFlops Xeon, 327 TFlops GPU et 27 TFlops Xeon Phi KNL.

--> [Description détaillée](description-detaillee.md)

--> [Documentation utilisateur](utilisation/index.md)

## Cadre général

Myria est conçue pour une utilisation en mode partagé, avec environ 300 comptes utilisateurs. Les travaux sont soumis en batch au travers de l’environnement de soumission `Slurm`, depuis les frontales de connexion vers les nœuds de calcul. L’interactif sur les frontales est limité aux compilations, éditions, soumission et aux petits et courts tests des codes.



Le calculateur est administré par l’équipe du Criann, les commandes Linux d’administration ne sont donc pas autorisées pour les utilisateurs. Les logiciels sont installés par l’équipe du Criann, sur demande, et sont accessibles au travers des commandes `module`.



Tous ces éléments sont détaillés dans la rubrique [guide d’utilisation](utilisation/index.md), qui décrit le mode de fonctionnement standard du service. Des adaptations sont possibles pour lever les limitations rencontrées (exemple : adéquation du stockage, installation d’un logiciel supplémentaire), sur demande auprès de <support@criann.fr>

## Suivi des consommations

Le suivi de consommation est disponible en ligne de commande si vous possédez un compte sur le calculateur Myria via la commande `maconso`. Si vous êtes responsable de projet ou responsable adjoint, l'option `-u` permet d'afficher le détail pour tous les comptes.

## Remerciements

Les publications reposant sur des résultats obtenus dans le cadre des attributions d'heures devront mentionner la mise à disposition des moyens par le CRIANN. Formulation à titre indicatif :

- Ce travail a bénéficié des moyens de calcul du mésocentre CRIANN (Centre Régional Informatique et d’Applications Numériques de Normandie).
- Part of this work / The present work / was performed using computing resources of CRIANN (Normandy, France)

## Modalités d'accès, autres documents

Les modalités d'accès aux ressources du calculateur MYRIA sont décrites sur le [site de présentation du Criann](http://www.criann.fr), où sont également disponibles les [formulaires](https://www.criann.fr/formulaires/) d'ouverture de compte. 

Différents documents de présentation et rapports d'activités sont disponibles sur [cette page](https://www.criann.fr/documents-presentation/). 