---
title: "Modélisation Moléculaire"
---
# Modélisation Moléculaire

## Les laboratoires normands

Les laboratoires des établissements publics normands utilisant la modélisation moléculaire dans leur recherche ont accès aux licences de logiciels spécialisés (bases de données réactionnelles, modélisation).

Ce service s’inscrit dans la continuité du projet de Réseau Normand pour la Modélisation Moléculaire (RNMM), démarré au printemps 1997 et financé par la communauté Européenne (Feder). Le RNMM avait alors permis aux laboratoires de recherche en chimie, biologie et santé du territoire normand (Rouen, Evreux, Le Havre et Caen) de s’équiper en stations de travail et d'acquérir des logiciels spécialisés au travers du Criann dans une démarche de mutualisation.

Les équipes utilisatrices sont principalement rattachées à des unités mixtes associant le CNRS ou l'INSERM avec les universités et écoles d'ingénieurs normandes (laboratoires COBRA, CERMN, LEMA, SMS, PBS...). Une liste des laboratoires normands est disponible sur les sites institionnels de [Normandie Université](http://www.normandie-univ.fr/annuaire-des-unites-et-structures-de-recherche-en-normandie-593.kjsp), du [CNRS](https://normandie.cnrs.fr) ou encore de l'[IRIB](https://irib.univ-rouen.fr).

## Les logiciels de modélisation moléculaire

### 3DS BIOVIA Accelrys

Les logiciels Dassault Biovia (ex Accelrys) fonctionnent avec des licences à jetons. Le CRIANN possède des jetons qui sont distribués au fur et à mesure des demandes par le serveur de licences :

- 4 jetons pour Discovery Studio "Academic Standard Complete"
- 2 jetons pour Material Studio "Academic Standard Base"
- 2 jetons pour Material Studio "Academic Classical & Mesoscale"

Toute machine du Réseau Normand de Modélisation Moléculaire peut accéder aux logiciels, après validation par l'équipe technique du CRIANN.

### Gaussian

Gaussian 03 fonctionne sur le supercalculateur Myria pour les utilisateurs académiques. Les versions plus récentes n'ont pas été acquises par le CRIANN

### Schrödinger

Les logiciels Schrödinger fonctionnent également avec un système de 50 jetons.
Les logiciels Schrödinger :

- Jaguar
- Maestro
- Knime

Jaguar est également disponible sur le supercalculateur Myria pour les utilisateurs académiques.

### Matrix Science

Le logiciel Mascot est hébergé sur un serveur du CRIANN et est interrogeable à partir de machines des laboratoires du projet. La licence permet de lancer simultanément 3 calculs.
