---
title: Gramc
---
# Gramc


**Gramc** (Gestion des Ressources et leur Attribution pour Mésocentres de Calcul) permet aux responsables de projet d’effectuer leurs demandes d'attribution de ressource au moyen d’un formulaire en ligne. Cet outil est le fruit d’un développement du mésocentre de calcul intensif Calmip de Toulouse. 

Sa version Criann est mise en service à l’occasion du lancement du premier appel à projets scientifique 2022, le 8 novembre 2021.

Lors de sa mise en service, [Gramc-Criann](https://gramc.criann.fr) contient la base des projets actifs et des utilisateurs déjà connus du service de calcul du Criann.

## Utilisation de la plateforme [Gramc-Criann](https://gramc.criann.fr)

- La plateforme est accessible aux utilisateurs authentifiés au travers de la Fédération d'identité Education-Recherche.
- L’outil se base sur les informations de nom, prénom et adresse email issus de la Fédération.
    - En cas d'appartenances multiples, un porteur ou collaborateur de projet scientifique doit privilégier l'établissement pour lequel son adresse email est celle qui est connue du Criann (indiquée lors de la demande de compte ou du dépôt de projet sur le calculateur). Il accède alors depuis Gramc à ses projets actifs.
- Le laboratoire de rattachement est à sélectionner dans un menu déroulant : en cas de laboratoire non encore répertorié sur [Gramc-Criann](https://gramc.criann.fr), contacter le Criann en écrivant à admin@criann.fr
- Un [guide d'utilisation de Gramc-Criann](https://services.criann.fr/services/calcul/cluster-myria/documents/gramc-criann-guide.pdf) est disponible.
- Les demandes d’ouverture de compte sur le calculateur s’effectuent au moyen des formulaires décrits sur https://www.criann.fr/acces-calcul/
    - À terme, un développement sera effectué pour pré-remplir le formulaire avec les informations issues de la Fédération (en particulier l'adresse email).
    - En attendant la procédure suivante est conseillée :
        - Connexion sur Gramc pour récupérer ces informations
        - Saisie manuelle à l’identique dans le formulaire.

!!! warning "Dans la version actuelle de [Gramc-Criann](https://gramc.criann.fr)"
    La déclaration d’un collaborateur (partie "I. Présentation" / "Personnes participant au projet") ne sert qu’à déclarer une personne devant participer à la rédaction de la demande d’attribution.  
    
    A ce jour, pour ajouter des collaborateurs à une demande d'attribution, il faut au préalable que ceux-ci s'authentifient au moins une fois et activent leurs comptes sur ce portail afin que leurs profils soient connus et que vous puissiez les ajouter en tant que collaborateurs.

## Liens utiles

- [Modalités d'accès aux ressources](https://www.criann.fr/acces-calcul/) (cette page comprend le formulaire d'ouverture de compte sur calculateur, et la charte générale du Criann)
- [Conditions Générales d'Utilisation](https://www.criann.fr/cgu-calcul/) du service de calcul du Criann